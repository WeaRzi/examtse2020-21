import random
import pandas as pd
import numpy as np


####################################
# Products sold classes ############
####################################

# Drinks
class Drink(object):
    def __init__(self, drink_price):
        # We set basic characteristics of the drink
        self.price = drink_price


# Food
class Food(object):
    def __init__(self, food_price):
        # We set basic characteristics of the food
        self.price = food_price


####################################
# Customer classes #################
####################################

# We create a general customer class
class Customer(object):
    # We set the first identifier
    class_id = 'CID0'

    def __init__(self):
        # It is characterized by a unique identifier
        # Each time we will create a customer it will add 1 to the number in the identifier previously created
        self.customer_id = Customer.class_id
        Customer.class_id = Customer.class_id[:3] + str(int(Customer.class_id[3:]) + 1)

        # We set an empty list for each customer that will host its purchase history
        self.history_drink_purchases = []
        self.history_food_purchases = []

    def purchase(self, date, df_probability_drink, df_probability_food, dictionary_products):
        hour = date[11:]
        # We set corresponding probabilities to each food and drink
        drink_probabilities = df_probability_drink.loc[
            df_probability_drink['HOUR'] == hour, 'probability'].values
        food_probabilities = df_probability_food.loc[
            df_probability_food['HOUR'] == hour, 'probability'].values

        # We set different drinks and foods offered to the customers
        drink_choices_offered = df_probability_drink['product'].unique()
        food_choices_offered = df_probability_food['product'].unique()

        # The customer makes a random choice based on probabilities
        self.drink_choice = random.choices(drink_choices_offered, weights=drink_probabilities)
        self.food_choice = random.choices(food_choices_offered, weights=food_probabilities)

        # We find corresponding prices of customer's choice
        # We refer to drink and food classes as strings so we need to use eval function to consider it as an object
        self.drink_price = dictionary_products[self.drink_choice[0]].price
        self.food_price = dictionary_products[self.food_choice[0]].price

        # We add customer purchase to its history
        self.history_drink_purchases.append(self.drink_choice[0])
        self.history_food_purchases.append(self.food_choice[0])

        # We update its budget
        self.budget -= (self.drink_price + self.food_price)

    def give_purchase(self):
        return self.drink_choice[0], self.food_choice[0]


# We create a class for one time customers
class OneTimeCustomer(Customer):
    def __init__(self):
        # We set the customer id
        self.customer_id = Customer.class_id
        Customer.class_id = Customer.class_id[:3] + str(int(Customer.class_id[3:]) + 1)

        # We set the customer starting budget
        self.budget = 100

        # We set an empty list for each customer that will host its purchase history
        self.history_drink_purchases = []
        self.history_food_purchases = []


# Regular one time customers
class RegularOneTimeCustomer(OneTimeCustomer):
    def give_amount(self):
        return self.drink_price + self.food_price


# Tripadvisor one time customers
class TripadvisorOneTimeCustomer(OneTimeCustomer):
    def give_amount(self):
        tip = random.randint(1, 10)
        return self.drink_price + self.food_price + tip, tip


# We create a class for returning customers
# There are two types of returning customers: regular and hipsters
class ReturningCustomer(Customer):
    def __init__(self):
        # We set the customer id
        self.customer_id = Customer.class_id
        Customer.class_id = Customer.class_id[:3] + str(int(Customer.class_id[3:]) + 1)

        # We set an empty list for each customer that will host its purchase history
        self.history_drink_purchases = []
        self.history_food_purchases = []

    def give_history(self):
        print(f'Drink purchases: {self.history_drink_purchases}')
        print(f'Food purchases: {self.history_food_purchases}')

    def give_amount(self):
        return self.drink_price + self.food_price


# Regular returning customer class
class RegularReturningCustomer(ReturningCustomer):
    def __init__(self):
        # We set the customer id
        self.customer_id = Customer.class_id
        Customer.class_id = Customer.class_id[:3] + str(int(Customer.class_id[3:]) + 1)

        # We create an empty list that will host customer's purchase
        self.history_drink_purchases = []
        self.history_food_purchases = []

        # We set its starting budget
        self.budget = 250


# Hipster class
class HipsterReturningCustomer(ReturningCustomer):
    def __init__(self):
        # We set the customer id
        self.customer_id = Customer.class_id
        Customer.class_id = Customer.class_id[:3] + str(int(Customer.class_id[3:]) + 1)

        # We create an empty list that will host customer's purchase
        self.history_drink_purchases = []
        self.history_food_purchases = []

        # We set its starting budget
        self.budget = 500

