import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import datetime as dt

# Import coffee bar data
coffeebar = pd.read_csv('../examtse2020-21/Data/Coffeebar_2016-2020.csv', sep=";")

# Look at 5 first  and last rows of the data frame
coffeebar.head()
coffeebar.tail()

# Convert columns to desired types
coffeebar['TIME'] = pd.to_datetime(coffeebar['TIME'])
coffeebar['DRINKS'] = coffeebar['DRINKS'].astype('category')
coffeebar['FOOD'] = coffeebar['FOOD'].astype('category')

# Now we look at the types of foods and drinks sold at the coffeebar
# Types of foods:
coffeebar['FOOD'].unique()  # There are 4 different foods: sandwich, pie, muffin and cookie
# Types of drinks:
coffeebar['DRINKS'].unique()  # There are 6 different drinks: frappucino, soda, coffee, tea, water and milkshake
# Number of different customer
coffeebar['CUSTOMER'].nunique()  # There are 247988 unique customers

################################################
# FOOD AND DRINKS VISUALIZATIONS ###############
################################################
# Let's look at the number of each type of food sold in the whole period (plot1)
coffeebar['FOOD'].value_counts().plot.bar()
plt.title('Total number of food sold by type')
plt.ylabel('Number of sales')
plt.xticks(rotation=0)
plt.savefig('../examtse2020-21/Results/Plots/plot1.png')
plt.show()
# We can see that sandwich is the food that was most sold (twice as much as other foods).

# Now we look at drinks (plot2)
coffeebar['DRINKS'].value_counts().plot.bar()
plt.title('Total number of drinks sold by type')
plt.ylabel('Number of sales')
plt.xticks(rotation=0)
plt.savefig('../examtse2020-21/Results/Plots/plot2.png')
plt.show()
# We can see that sodas are the drinks that were most sold, followed by coffee.


# Now we ask ourselves if the coffeeshop sold more drinks than foods over the five years
# We count total sales of drinks and food
n_drinks = coffeebar['DRINKS'].count()
n_foods = coffeebar['FOOD'].count()
plt.bar(['DRINKS', 'FOODS'], [n_drinks, n_foods])
plt.savefig('../examtse2020-21/Results/Plots/total_drinks_food.png')
plt.show()
# We can see that the coffeeshop sells much more drinks than foods. It sold two time more drinks than foods.


# Let's see if there are some products that are more sold at particular periods
coffeebar['YEAR'] = coffeebar['TIME'].dt.year
coffeebar['MONTH'] = coffeebar['TIME'].dt.month
coffeebar['DAY'] = coffeebar['TIME'].dt.day

# We count the number of products sold by month and category
foods_by_month = coffeebar.groupby('MONTH')['FOOD'].value_counts().reset_index(name='count')
drinks_by_month = coffeebar.groupby('MONTH')['DRINKS'].value_counts().reset_index(name='count')
# We make the line plot
sns.lineplot(foods_by_month['MONTH'].astype('category'), foods_by_month['count'], hue=foods_by_month['FOOD'])
plt.title('Food sales by month and category')
plt.ylabel('Number of sales')
plt.savefig('../examtse2020-21/Results/Plots/line_plot_food.png')
plt.show()
# We see that the coffeebar sells less food on february

# Same plot for drinks
sns.lineplot(drinks_by_month['MONTH'].astype('category'), drinks_by_month['count'], hue=drinks_by_month['DRINKS'])
plt.title('Drinks sales by month and category')
plt.ylabel('Number of sales')
plt.savefig('../examtse2020-21/Results/Plots/line_plot_drinks.png')
plt.show()
# Same observation than for the food. We also notice that coffee has less variation than soda.


# Now we interest ourselves to returning clients
sales_by_id = pd.DataFrame(coffeebar['CUSTOMER'].value_counts()).reset_index()
sales_by_id.columns = ['CUSTOMER', 'sales_count']
returning_customers = sales_by_id.loc[sales_by_id['sales_count'] > 1]

# We plot the top 30 clients by ID
plt.bar(returning_customers.head(30)['CUSTOMER'].astype('category'), returning_customers.head(30)['sales_count'])
plt.xticks(rotation=90)
plt.title('Top 30 returning customers')
plt.ylabel('Number of sales to each customer')
plt.savefig('../examtse2020-21/Results/Plots/top_30_customers.png')
plt.show()

# We plot the density of the number of sales by returning clients
# We compute the average to show it in the density plot
returning_customers_mean_sales = int(returning_customers['sales_count'].mean())
# Density plot
plt.subplot(211)
sns.distplot(returning_customers['sales_count'])
# We add the average line
plt.axvline(returning_customers_mean_sales, color='red', linestyle='--',
            label=f'Average = {returning_customers_mean_sales}')
plt.title('Distribution of the number of sales by returning customers')
plt.xlabel('')
plt.legend(loc='upper right')
# Boxplot
plt.subplot(212)
plt.boxplot(returning_customers['sales_count'], vert=False)
plt.xlabel('Number of sales to returning customers')
plt.savefig('../examtse2020-21/Results/Plots/returning_customers_distribution.png')
plt.show()
# We can see that the distributions is bell-shaped and looks like normal.
# In average, regular clients bought 65 times over the 5 years
# Regular clients bought a minimum of 40 and a maximum of 88 over the five years


################################################
# PROBABILITIES ################################
################################################

# We extract the hour from the timestamp
coffeebar['HOUR'] = coffeebar['TIME'].dt.time


# We are going to loop over the different types of food and drinks by creating a data frame having as rows
# the time slots and as columns the probability to buy each category of food or drinks.
# At each iteration we create a new column containing the probability of buying a certain food/drink at a given time.
# We know that the coffee bar opens every day and sells at the same time slots every day so the total number of possible
# sales at a given time is equal to the number of days (here 1825).
# We define the probability to buy product x at time t as:
# P(buy x at time t) = sum(sales of x at t) / number of days
# We also have to consider the probability NOT to buy, especially for food. We define it as:
# P(not to buy at time t) = (number of days - sum(food/drink sales at t)) / number of days
def compute_probabilities(type_of_sale):
    # Total days
    n_days = coffeebar.groupby('HOUR')['HOUR'].count()
    # Total sales by time slot
    n_sales = coffeebar.groupby('HOUR')[type_of_sale].count()
    # Create data frame
    table_probabilities = pd.DataFrame({'total_days': n_days, 'total_sales': n_sales})
    # Different category names
    different_categories = coffeebar[type_of_sale].dropna().unique()
    # Loop over category names
    for selected_category in different_categories:
        # Number of sales by hour for selected category
        n_category = coffeebar.loc[coffeebar[type_of_sale] == selected_category].groupby('HOUR').count()[type_of_sale]
        # Compute probabilities
        table_probabilities[selected_category] = n_category / n_days
    # Compute the probability not to buy
    table_probabilities['nothing'] = (n_days - n_sales) / n_days
    return table_probabilities.fillna(0)


# We apply the function to drinks and food
table_probabilities_food = compute_probabilities('FOOD')
table_probabilities_drinks = compute_probabilities('DRINKS')

# We check that the sum of the probabilities for a given time is equal to 1
table_probabilities_food.drop(['total_days', 'total_sales'], axis=1).sum(axis=1).unique()
table_probabilities_drinks.drop(['total_days', 'total_sales'], axis=1).sum(axis=1).unique()
# There are only ones in the sum of the probabilities so it should be ok.
# We can also check that there is a probability 0 not to buy a drink.
table_probabilities_drinks['nothing'].sum()
# The sum is equal to 0 so it's also ok.

# We export the tables to our results folder
table_probabilities_food.to_csv('../examtse2020-21/Results/Tables/probabilities_food.csv')
table_probabilities_drinks.to_csv('../examtse2020-21/Results/Tables/probabilities_drinks.csv')


