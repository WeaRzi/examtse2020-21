from Code import Part_2 as partwo
import pandas as pd
import numpy as np
import random
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.lines import Line2D
import seaborn as sns

pd.set_option('display.max_columns', 500)

coffeebar = pd.read_csv('../examtse2020-21/Data/Coffeebar_2016-2020.csv', sep=';')
probabilities_drinks = pd.read_csv('../examtse2020-21/Results/Tables/probabilities_drinks.csv')
probabilities_food = pd.read_csv('../examtse2020-21/Results/Tables/probabilities_food.csv')


# We prefer a long shaped data frame rather than a wide shape
# We create a function that transforms the data frame and creates columns 'product' and 'probability'
def wide_to_long(df):
    value_vars = [x for x in df.columns if x not in ['HOUR', 'total_days', 'total_sales']]
    long_df = df.melt(
        id_vars=['HOUR', 'total_days', 'total_sales'],
        value_vars=value_vars,
        value_name='probability',
        var_name='product'
    )
    return long_df


# We transform both data frames
probabilities_drinks_long = wide_to_long(probabilities_drinks)
probabilities_food_long = wide_to_long(probabilities_food)

# We create drink and food objects
# Drinks
frappucino = partwo.Drink(4)
soda = partwo.Drink(3)
coffee = partwo.Drink(3)
tea = partwo.Drink(3)
water = partwo.Drink(2)
milkshake = partwo.Drink(5)

# Food
sandwich = partwo.Food(2)
pie = partwo.Food(3)
muffin = partwo.Food(3)
cookie = partwo.Food(2)
nothing = partwo.Food(0)

# We create a dictionary of all the coffeebar products
dictionary_products = {'frappucino': frappucino, 'soda': soda, 'coffee': coffee, 'tea': tea, 'water': water,
                       'nothing': nothing, 'milkshake': milkshake, 'sandwich': sandwich, 'pie': pie, 'muffin': muffin,
                       'cookie': cookie}

# We create the 1000 returning customers, where 1/3 are hipsters and the rest regular
returning_customers = []

for i in range(0, 1000):
    if i < 333:
        returning_customers.append(partwo.HipsterReturningCustomer())
    else:
        returning_customers.append(partwo.RegularReturningCustomer())


# We first create a function that selects a random customer based on given probabilities
def select_random_customer(probabilities_otc_rc):
    # Random choice between One time and returning customers
    onetime_or_returning = random.choices(['OneTime', 'Returning'], weights=probabilities_otc_rc)

    # If choice = one time customer, then choose between regular or tripadvisor customer
    if onetime_or_returning[0] == 'OneTime':
        tripadvisor_or_regular = random.choices(['Regular', 'Tripadvisor'], weights=(0.9, 0.1))
        # If regular one time customer, create the customer
        if tripadvisor_or_regular[0] == 'Regular':
            customer = partwo.RegularOneTimeCustomer()
        # Else create a tripadvisor customer
        else:
            customer = partwo.TripadvisorOneTimeCustomer()
    # If choice = returning customer, then choose a random customer from our returning customer list
    else:
        customer = random.choices(returning_customers)[0]
    # We want to select only customers that have a budget high enough to buy the most expensive combination of drink
    # and food
    if customer.budget >= 8:
        return customer
    else:
        return select_random_customer(probabilities_otc_rc)


# Then we create a function that will return all information from a purchase at a given time:
# id of the customer, the drink and food he bought, the amount he paid and the possible tip he gave (if tripadvisor)
def make_purchase(time, part3=True):
    hour = time[11:]

    # Select randomly a customer with the function previously created
    if part3:
        customer = select_random_customer((0.8, 0.2))
    else:
        # We find the corresponding probability of having a one time or returning customer based on the hour
        p_rc = probabilities_by_type.loc[probabilities_by_type['time_slot'].astype(str) == hour, 'p_rc'].values[0]
        customer = select_random_customer((1 - p_rc, p_rc))

    # The customer makes a purchase at given time
    customer.purchase(time, probabilities_drinks_long, probabilities_food_long, dictionary_products)

    # We extract all information from the purchase
    drink = customer.give_purchase()[0]
    food = customer.give_purchase()[1]
    amount_spent = customer.give_amount()
    customer_id = customer.customer_id
    budget = customer.budget

    # We check if the customer gave a tip and set tip = 0 if he didn't give any
    # We transform amount_spent into a string as the length of a tuple with only one integer does not exist
    if type(amount_spent) == tuple:
        # We transform the string into an integer and we subtract the amount of the tip
        purchase_price = int(amount_spent[0] - amount_spent[1])
        tip = int(amount_spent[1])
    else:
        purchase_price = int(amount_spent)
        tip = 0
    return customer_id, drink, food, purchase_price, tip, budget, customer


# Try the function
make_purchase('2016-01-01 16:00:00', part3=True)


# We create a function that generates the simulated data
def generate_simulated_data(df, part3, funct):
    # Apply the general function to each row
    df['total_information'] = df.apply(lambda x: funct(x['time'], part3=part3), axis=1)

    # Put each information to the respective column
    df['customer'] = df.apply(lambda x: x['total_information'][0], axis=1)
    df['drinks'] = df.apply(lambda x: x['total_information'][1], axis=1)
    df['food'] = df.apply(lambda x: x['total_information'][2], axis=1)
    df['amount_spent'] = df.apply(lambda x: x['total_information'][3], axis=1)
    df['tip'] = df.apply(lambda x: x['total_information'][4], axis=1)
    df['budget'] = df.apply(lambda x: x['total_information'][5], axis=1)
    df['object'] = df.apply(lambda x: x['total_information'][6], axis=1)

    return df


# We create the data frame with the time range
simulation = pd.DataFrame({'time': coffeebar['TIME']})
# We generate the simulated data (it takes some minutes)
simulation = generate_simulated_data(simulation, True, funct=make_purchase)

# We export the simulated data
simulation.to_csv('../examtse2020-21/Results/Tables/simulation_final_table.csv')

# We check if the data frame is as expected
simulation.head()
simulation.tail()
simulation.food.unique()
simulation.drinks.unique()

##########
# PART 4 #
##########

##########################################################################
# Show some buying histories of returning customers for your simulations #
##########################################################################


# We want to count each drink and food types each returning customer bought
# We create a function that gives the quantity bought for a given product and a given customer
def number_purchases(customer, product):
    if product in ['coffee', 'water', 'milkshake', 'tea', 'soda', 'frappucino']:
        hist = customer.history_drink_purchases
    else:
        hist = customer.history_food_purchases
    dict_count = pd.Series(hist).value_counts().to_dict()
    return dict_count.get(product)


# We create the data frame that will host de quantities bought
# First we filter returning customers
rc_simulation = simulation.loc[simulation['customer'].duplicated()]
# Then we create a table with each customer and the object associated
history_purchases1 = pd.DataFrame({'customer': rc_simulation['customer'], 'object': rc_simulation['object']})\
    .drop_duplicates().reset_index(drop=True)

# We count the quantity bought by each customer for each product:
# We loop over the products and apply the functions we created previously
for product in dictionary_products.keys():
    history_purchases1[product] = history_purchases1.apply(lambda x: number_purchases(x['object'], product), axis=1)

history_purchases1.head()


# We want to plot a summary of a given customer history purchase
def plot_history_purchase(customer):
    # We set the figure
    fig = plt.figure(figsize=(15, 10))
    gs = gridspec.GridSpec(2, 2, figure=fig)

    # First plot: percentage of each drink type purchased
    ax1 = fig.add_subplot(gs[0, 0])
    pd.Series(customer.history_drink_purchases).value_counts(normalize=True) \
        .plot.bar(ax=ax1, color='lightgreen')
    plt.gca().set_yticklabels(['{:.0f}%'.format(x * 100) for x in plt.gca().get_yticks()])
    plt.title('Drink purchases', fontsize=9)
    plt.xticks(rotation=45)

    # Second plot: percentage of each food type purchased
    ax2 = fig.add_subplot(gs[1, 0])
    pd.Series(customer.history_food_purchases).value_counts(normalize=True) \
        .plot.bar(ax=ax2, color='lightblue')
    plt.gca().set_yticklabels(['{:.0f}%'.format(x * 100) for x in plt.gca().get_yticks()])
    plt.title('Food purchases', fontsize=9)
    plt.xticks(rotation=45)

    # Third plot: distribution of the amount spent at each purchase
    ax3 = fig.add_subplot(gs[:, 1])
    amounts = simulation.loc[simulation.object == customer, 'amount_spent']
    sns.distplot(amounts, ax=ax3, color='lightcoral')
    plt.title('Distribution of the amount spent', fontsize=9)
    plt.xlabel('Amount spent')

    # Show the plot
    id_customer = simulation.loc[simulation['object'] == customer, 'customer'].values[0]
    fig.suptitle(id_customer)
    plt.savefig(f'../examtse2020-21/Results/Plots/history_purchase_{id_customer}')
    plt.show()


plot_history_purchase(history_purchases1.iloc[5, 1])
plot_history_purchase(history_purchases1.iloc[100, 1])
plot_history_purchase(history_purchases1.iloc[997, 1])

##########################################################################
# Returning customers of initial data set ################################
##########################################################################

coffeebar['HOUR'] = pd.to_datetime(coffeebar['TIME']).dt.hour
coffeebar['TIME_SLOT'] = pd.to_datetime(coffeebar['TIME']).dt.time

# Returning customers
coffeebar_rc = coffeebar.loc[coffeebar['CUSTOMER'].duplicated()]
print(f'There are {coffeebar_rc["CUSTOMER"].nunique()} returning customers in the provided data set.')

# One time customers
coffeebar_otc = coffeebar.loc[~coffeebar['CUSTOMER'].duplicated()]
print(f'There are {coffeebar_otc["CUSTOMER"].nunique()} one time customers in the provided data set.')

# Visualizing when returning customers show up more
coffeebar_rc['HOUR'].value_counts().sort_index().plot.bar()
plt.title('Number of returning customers per hour over the 5 years')
plt.xticks(rotation=0)
plt.xlabel('Hour')
plt.ylabel('Number of customers')
plt.savefig('../examtse2020-21/Results/Plots/nb_returning_customers_per_hour.png')
plt.show()

# Visualizing when one-time customers show up more
coffeebar_otc['HOUR'].value_counts().sort_index().plot.bar()
plt.title('Number of one-time customers per hour over the 5 years')
plt.xticks(rotation=0)
plt.xlabel('Hour')
plt.ylabel('Number of customers')
plt.savefig('../examtse2020-21/Results/Plots/nb_one_time_customers_per_hour.png')
plt.show()

# Compute probabilities of having a returning or one-time customers for a given time
# We first count the number of each type of customer per hour
probabilities_by_type = pd.DataFrame(
    {'time_slot': coffeebar['TIME_SLOT'].unique(),
     'total': coffeebar['TIME_SLOT'].value_counts().sort_index(),
     'returning_customers': coffeebar_rc['TIME_SLOT'].value_counts().sort_index(),
     'one_time_customers': coffeebar_otc['TIME_SLOT'].value_counts().sort_index()})

# And we compute the probabilities
probabilities_by_type['p_rc'] = probabilities_by_type['returning_customers'] / probabilities_by_type['total']
probabilities_by_type['p_otc'] = probabilities_by_type['one_time_customers'] / probabilities_by_type['total']

probabilities_by_type.to_csv('../examtse2020-21/Results/Tables/probabilities_customers.csv', index=False)


##########################################################################
# Simulation 2 ###########################################################
##########################################################################

# Simulate again the data based on computed probabilities
# We generate again the returning customers
returning_customers = []

for i in range(0, 1000):
    if i < 333:
        returning_customers.append(partwo.HipsterReturningCustomer())
    else:
        returning_customers.append(partwo.RegularReturningCustomer())

simulation2 = pd.DataFrame({'time': coffeebar['TIME']})
simulation2 = generate_simulated_data(simulation2, part3=False, funct=make_purchase)
simulation2['time_slot'] = pd.to_datetime(simulation2['time']).dt.time

# Let's compare history purchases
rc_simulation2 = simulation2.loc[simulation2['customer'].duplicated()]
history_purchases2 = pd.DataFrame({'customer': rc_simulation2['customer'], 'object': rc_simulation2['object']}) \
    .drop_duplicates()

# We count each product
for product in dictionary_products.keys():
    history_purchases2[product] = history_purchases2.apply(lambda x: number_purchases(x['object'], product), axis=1)

# Compare quantities bought
# We want to compare the distribution of quantities bought by each returning customer for each product
for i, j in enumerate(dictionary_products.keys()):
    plt.subplot(4, 3, i + 1)
    sns.distplot(history_purchases1[j], color='red', hist=False)
    sns.distplot(history_purchases2[j], color='blue', hist=False)
    plt.title(j)
    plt.xlabel('')
lines = [Line2D([0], [0], color=c) for c in ['red', 'blue']]
labels = ['First simulation', 'Second simulation']
plt.figlegend(lines, labels, loc='lower right')
plt.tight_layout()
plt.savefig('../examtse2020-21/Results/Plots/distrib_quantities_sim1_sim2.png')
plt.show()

# Probability of returning customers to come vs probability to buy a sandwich
plt.scatter(probabilities_by_type['p_rc'], probabilities_food['sandwich'])
plt.xlabel('Probability for returning customers to come')
plt.ylabel('Probability to buy a sandwich')
plt.title('Probability for a returning customer to come vs buying a sandwich')
plt.savefig('../examtse2020-21/Results/Plots/p_returning_customer_vs_buy_sandwich.png')
plt.show()


# Correlations between returning and one time customers
# We create a function that gives the percentage of each product bought at a given time
def compute_percentages(df, type_of_product):
    quantities = df.groupby(['time_slot', type_of_product])['customer'].count().reset_index()
    quantities['pct_bought'] = quantities.groupby('time_slot').apply(lambda x: x['customer'] / x['customer'].sum()) \
        .reset_index()['customer']
    return quantities


def compare_rc_otc_percentages(simulated_data, type_of_product):
    # Percentage for returning customers
    pct_rc = compute_percentages(simulated_data.loc[simulated_data['customer'].duplicated()], type_of_product)
    # Percentage for one time customers
    pct_otc = compute_percentages(simulated_data.loc[~simulated_data['customer'].duplicated()], type_of_product)
    # We merge the tables
    df_pct = pd.merge(pct_rc, pct_otc, on=['time_slot', type_of_product], suffixes=['_rc', '_otc'])
    return df_pct


pct_drinks2 = compare_rc_otc_percentages(simulation2, 'drinks')
# We plot the correlation
sns.scatterplot('pct_bought_rc', 'pct_bought_otc', data=pct_drinks2, hue='drinks')
plt.xlabel('RETURNING customers')
plt.ylabel('ONE TIME customers')
plt.title('Proportion of each drink bought at a given time')
plt.savefig('../examtse2020-21/Results/Plots/correlation_drinks_sim2.png')
plt.show()

# We do the same for food
pct_food2 = compare_rc_otc_percentages(simulation2, 'food')
sns.scatterplot('pct_bought_rc', 'pct_bought_otc', data=pct_food2, hue='food')
plt.xlabel('RETURNING customers')
plt.ylabel('ONE TIME customers')
plt.title('Proportion of each food bought at a given time')
plt.savefig('../examtse2020-21/Results/Plots/correlation_food_sim2.png')
plt.show()


##########################################################################
# Simulation 3 ###########################################################
##########################################################################

# Now we have only 50 returning customers
returning_customers = []

for i in range(0, 50):
    if i < 50 / 3:
        returning_customers.append(partwo.HipsterReturningCustomer())
    else:
        returning_customers.append(partwo.RegularReturningCustomer())

simulation3 = pd.DataFrame({'time': coffeebar['TIME']})
simulation3 = generate_simulated_data(simulation3, part3=True, funct=make_purchase)
simulation3['time_slot'] = pd.to_datetime(simulation3['time']).dt.time

# We want to compare total income on the period of each simulation
income1 = simulation['amount_spent'].sum()
income2 = simulation2['amount_spent'].sum()
income3 = simulation3['amount_spent'].sum()

plt.bar(['Simulation1', 'Simulation2', 'Simulation3'], [income1, income2, income3])
plt.title('Comparison of total income')
plt.savefig('../examtse2020-21/Results/Plots/comparison_income_123.png')
plt.show()

# We should have more tripadvisor customers so more tips
tip1 = simulation['tip'].sum()
tip2 = simulation2['tip'].sum()
tip3 = simulation3['tip'].sum()

plt.bar(['Simulation1', 'Simulation2', 'Simulation3'], [tip1, tip2, tip3])
plt.title('Comparison of total tips received')
plt.savefig('../examtse2020-21/Results/Plots/comparison_tip_123.png')
plt.show()

# Let's see if we still have the same correlation as before
pct_drinks3 = compare_rc_otc_percentages(simulation3, 'drinks')
sns.scatterplot('pct_bought_rc', 'pct_bought_otc', data=pct_drinks3, hue='drinks')
plt.xlabel('RETURNING customers')
plt.ylabel('ONE TIME customers')
plt.title('Proportion of each drink bought at a given time')
plt.savefig('../examtse2020-21/Results/Plots/correlation_drinks_sim3.png')
plt.show()
# Now the relationship is less linear than before
# Returning customers have more variance, especially for soda and coffee

# We look at the proportion of returning customers in total customers by year
# We create a dummy for returning customers
simulation3['customer_type'] = np.where(simulation3['customer'].duplicated(), 'returning', 'one time')
# Since we want by year we extract it from the time column
simulation3['year'] = pd.to_datetime(simulation3['time']).dt.year
# We count the number of times each type came by year
simulation3.groupby('year')['customer_type'].value_counts()


##########################################################################
# Simulation 4 ###########################################################
##########################################################################

# Prices change from 2018 and increase by 20%
# We will slightly modifiy the make purchase function
def make_purchase2(time, part3=True):
    hour = time[11:]

    if pd.to_datetime(time).year < 2018:
        frappucino = partwo.Drink(4)
        soda = partwo.Drink(3)
        coffee = partwo.Drink(3)
        tea = partwo.Drink(3)
        water = partwo.Drink(2)
        milkshake = partwo.Drink(5)
        sandwich = partwo.Food(2)
        pie = partwo.Food(3)
        muffin = partwo.Food(3)
        cookie = partwo.Food(2)
        nothing = partwo.Food(0)
    else:
        frappucino = partwo.Drink(4 + 4*.2)
        soda = partwo.Drink(3 + 3*.2)
        coffee = partwo.Drink(3 + 3*.2)
        tea = partwo.Drink(3 + 3*.2)
        water = partwo.Drink(2 + 2*.2)
        milkshake = partwo.Drink(5 + 5*.2)
        sandwich = partwo.Food(2 + 2*.2)
        pie = partwo.Food(3 + 3*.2)
        muffin = partwo.Food(3 + 3*.2)
        cookie = partwo.Food(2 + 2*.2)
        nothing = partwo.Food(0 + 0*.2)

    # We create a dictionary of all the coffeebar products
    dictionary_products = {'frappucino': frappucino, 'soda': soda, 'coffee': coffee, 'tea': tea, 'water': water,
                           'nothing': nothing, 'milkshake': milkshake, 'sandwich': sandwich, 'pie': pie,
                           'muffin': muffin, 'cookie': cookie}

    # Select randomly a customer with function previously created
    if part3:
        customer = select_random_customer((0.8, 0.2))
    else:
        p_rc = probabilities_by_type.loc[probabilities_by_type['time_slot'].astype(str) == hour, 'p_rc'].values[0]
        customer = select_random_customer((1 - p_rc, p_rc))

    # The customer makes a purchase at given time
    customer.purchase(time, probabilities_drinks_long, probabilities_food_long, dictionary_products)

    # We extract all information from the purchase
    drink = customer.give_purchase()[0]
    food = customer.give_purchase()[1]
    amount_spent = customer.give_amount()
    customer_id = customer.customer_id
    budget = customer.budget

    # We check if the customer gave a tip and set tip = 0 if he didn't give any
    if type(amount_spent) == tuple:
        # We transform the string into an integer
        purchase_price = amount_spent[0] - amount_spent[1]
        tip = int(amount_spent[1])
    else:
        purchase_price = amount_spent
        tip = 0
    return customer_id, drink, food, purchase_price, tip, budget, customer


# We generate again the returning customers
returning_customers = []

for i in range(0, 1000):
    if i < 1000 / 3:
        returning_customers.append(partwo.HipsterReturningCustomer())
    else:
        returning_customers.append(partwo.RegularReturningCustomer())

# We simulate again the data with the modified function
simulation4 = pd.DataFrame({'time': coffeebar['TIME']})
simulation4 = generate_simulated_data(simulation4, part3=True, funct=make_purchase2)
simulation4['time_slot'] = pd.to_datetime(simulation4['time']).dt.time

# Let's see how total income evolved
income4 = simulation4['amount_spent'].sum()
plt.bar(['Simulation1', 'Simulation2', 'Simulation3', 'Simulation4'], [income1, income2, income3, income4])
plt.title('Comparison of total income')
plt.savefig('../examtse2020-21/Results/Plots/comparison_income_1234.png')
plt.show()
# We compute by how much total income increased with respect to the mean of the three previous simulations
increase_income = (income4 - np.mean([income1, income2, income3]))/np.mean([income1, income2, income3])*100
print(f'In simulation 4, total income increased by {int(increase_income)}%')


##########################################################################
# Simulation 5 ###########################################################
##########################################################################

# Budget of hipsters drops to 40
# We generate again returning customers
returning_customers = []

for i in range(0, 1000):
    if i < 1000 / 3:
        hipster = partwo.HipsterReturningCustomer()
        hipster.budget = 40
        returning_customers.append(hipster)
    else:
        returning_customers.append(partwo.RegularReturningCustomer())

simulation5 = pd.DataFrame({'time': coffeebar['TIME']})
simulation5 = generate_simulated_data(simulation5, part3=True, funct=make_purchase)
simulation5['time_slot'] = pd.to_datetime(simulation5['time']).dt.time

# Let's compare history purchases across the 6 simulations
history_purchases3 = pd.DataFrame(
    {'customer': simulation3.loc[simulation3['customer'].duplicated()]['customer'],
     'object': simulation3.loc[simulation3['customer'].duplicated()]['object']})\
    .drop_duplicates()
history_purchases4 = pd.DataFrame(
    {'customer': simulation4.loc[simulation4['customer'].duplicated()]['customer'],
     'object': simulation4.loc[simulation4['customer'].duplicated()]['object']})\
    .drop_duplicates()
history_purchases5 = pd.DataFrame(
    {'customer': simulation5.loc[simulation5['customer'].duplicated()]['customer'],
     'object': simulation5.loc[simulation5['customer'].duplicated()]['object']})\
    .drop_duplicates()

# We apply the number_purchases function to each table and each product
for df in [history_purchases3, history_purchases4, history_purchases5]:
    for product in dictionary_products.keys():
        df[product] = df.apply(lambda x: number_purchases(x['object'], product), axis=1)

# We make a subplot for each product
for i, j in enumerate(dictionary_products.keys()):
    plt.subplot(4, 3, i + 1)
    sns.distplot(history_purchases1[j], color='red', hist=False)
    sns.distplot(history_purchases2[j], color='blue', hist=False)
    sns.distplot(history_purchases3[j], color='green', hist=False)
    sns.distplot(history_purchases4[j], color='orange', hist=False)
    sns.distplot(history_purchases5[j], color='brown', hist=False)
    plt.title(j)
    plt.xlabel('')
lines = [Line2D([0], [0], color=c) for c in ['red', 'blue', 'green', 'orange', 'brown']]
labels = ['Simulation 1', 'Simulation 2', 'Simulation 3', 'Simulation 4', 'Simulation 5']
plt.figlegend(lines, labels, loc='lower right')
plt.tight_layout()
plt.savefig('../examtse2020-21/Results/Plots/distrib_quantities_12345.png')
plt.show()


##########################################################################
# Simulation 6 ###########################################################
##########################################################################

# Now we suppose that the pandemic forces the coffee bar to close outside lunch time
# Let's see how this impacts its revenue
returning_customers = []

for i in range(0, 1000):
    if i < 333:
        returning_customers.append(partwo.HipsterReturningCustomer())
    else:
        returning_customers.append(partwo.RegularReturningCustomer())

simulation6 = pd.DataFrame({'time': coffeebar.loc[pd.to_datetime(coffeebar['TIME']).dt.hour.between(11, 13)]['TIME']})
simulation6 = generate_simulated_data(simulation6, part3=True, funct=make_purchase)
simulation6['time_slot'] = pd.to_datetime(simulation6['time']).dt.time

# We want to see if the food buying pattern changes if the coffee bar is opened only for lunch
dfplot = pd.DataFrame({
    'food': sorted(simulation['food'].unique()),
    's1': simulation['food'].value_counts(normalize=True).sort_index().values,
    's2': simulation2['food'].value_counts(normalize=True).sort_index().values,
    's3': simulation3['food'].value_counts(normalize=True).sort_index().values,
    's4': simulation4['food'].value_counts(normalize=True).sort_index().values,
    's5': simulation5['food'].value_counts(normalize=True).sort_index().values,
    's6': simulation6['food'].value_counts(normalize=True).sort_index().values
})

dfplot.plot.bar()
plt.xticks(np.arange(5), sorted(simulation['food'].unique()))
plt.xticks(rotation=0)
plt.title('Proportion of each food bought by simulation')
plt.ylabel('Percentage of total food bought at a given simulation')
plt.savefig('../examtse2020-21/Results/Plots/food_buying_pattern_sim6.png')
plt.show()
