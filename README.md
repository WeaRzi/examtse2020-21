# Python project - EEE Master

Authors: Alexandre Arcens, Etienne Lapuente

### Files description
The repository contains four folders:
- Code: Code used to generate the setting, plots and tables from simulations.
- Data: Data given for first exploratory analysis
- Documentation: Given slides
- Results: plots and tables generated

### Context
In this project we consider the activity of a coffee bar that sells food and drinks.  
The coffee bar is open everyday from 8 am to 6 pm and make a sell every 4 or 5 minutes. 
It sells the following products:
- Drinks:
    - Coffee
    - Frappucino
    - Soda
    - Tea
    - Water
    - Milkshake
- Food:
    - Sandwich
    - Pie
    - Muffin
    - Cookie

We distinguish two groups of customers each one divided in two sub-groups and having specific characteristics:
- One time customers:
    - Regular customers
    - Tripadvisor customers: gives a random tip between 1 and 10 euros
- Returning customers:
    - Regular customers: 250 euros budget
    - Hipster customers: 500 euros budget

Returning customers can buy until their budget is at least equal to the sum of the maximum price of the combination drink-food.
Each purchase is based on a set of probabilities determining the
type of the customer, the tip he gives (if tripadvisor) and the choice he makes. He always buys a drink and sometimes a food.  
Each customer is able to tell the choice he made and the amount he spent and he keeps the history of his purchases.

### Objectives

The main objective here is to simulate a five year span. We will produce some exploratory analyis on a given dataset that 
will give us the food and drinks choice probabilities. Then we will simulate different situations each time changing a parameter in the setting.